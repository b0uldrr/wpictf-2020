## NotWannasigh

* **CTF:** WPI CTF 2020
* **Category:** rev
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 18/04/20

---

### Challenge
```
Please help! An evil script-kiddie (seriously, this is some bad code) was able to get this ransomware "NotWannasigh" onto one of our computers. The program ran and encrypted our file "flag.gif".

These are the resources we were able to gather for you:

- NotWannasigh.zip - the malicious ransomware executable
- flag-gif.EnCiPhErEd - our poor encrypted file that we need you to recover
- ransomNote.txt - the note left behind by the ransomware. I'm not sure you'll find anything usefull here
- 192-168-1-11_potential-malware.pcap - a packet capture that our IDS isolated, it seems that the program has some weird form of data exfiltration

We need you to reverse the malware and recover our flag.gif file. Good luck!

A note from the creator: Shoutout to Demonslay335 for challenge inspiration - he's done some awesome work in fighting ransomware. Also, the ransomware in this challenge is programmed to only target files named "flag.gif" so you shouldn't need to worry about the accidental execution, I just zipped it out of habit/good practice. Have fun and happy hacking!

Abjuri5t (John F.)

```

### Downloads
* [NotWannasigh.zip](NotWannasigh.zip) contains:
	* [NotWannasigh](NotWannasigh)
	* [flag-gif.EnCiPhErEd](flag-gif.EnCiPhErEd)
	* [ransomNote.txt](ransomNote.txt)
	* [192-168-1-11_potential-malware.pcap](192-168-1-11_potential-malware.pcap)

---

### Solution

The provided binary [NotWannasigh](NotWannasigh) finds a file (flag.gif), encrypts it and then deletes the unencrypted version. Our goal is to retreive the unencrypted file. We are given the malicious binary (NotWannasigh), the encrypted output file (flag-gif.EnCiPhErEd), a ransom note (ransomNote.txt) and a network pcap captured at the time of encryption (192-168-1-11_potential-malware.pcap).

I opened the binary in Ghidra and found that all of the action happens in the `main` function. I spent some time reading through the decompiled code and renaming variables to understand how it works. In short, NotWannasigh does the following: 

1. Creates a time object (line 33)
1. Uses that time object to seed `srand()` (line 35)
1. Creates a socket and then a TCP connection to 108.61.127.136 (line 51)
1. Sends the value of the time object to 108.61.127.136 (line 62)
1. Opens flag.gif (line 76)
1. Determines the number of bytes in flag.gif (line 81)
1. Creates a `for loop` that runs "number of bytes in flag.gif" times. During every loop, it:
	* Generates a random number with `rand()` (which is seeded by `srand` in step 2) (line 94)
	* Stores the **lowest byte** of that random number in an array (which I'll call `key_array`) (line 95)
	* Increments the `key_array` index with a counter variable (line 96)
1. Creates another loop which also runs for every byte in `flag.gif` (line 107). During every loop it:
	* Takes the next byte of `flag.gif` and XORs it with the next value in `key_array` (line 115)
	* Stores the output of that XOR in another array (which I'll call `encrypted_values`) (also line 115)
1. Deletes `flag.gif` (line 122)
1. Creates a new file `flag-gif.EnCiPhErEd` (line 125)
1. Loops through the `encrypted_values` array, storing each byte in `flag-gif.EnCiPhErEd` (lines 129 - 135)
1. Prints the "ransom note" on the screen

Looking through the pcap file ([192-168-1-11_potential-malware.pcap](192-168-1-11_potential-malware.pcap)), we can see the TCP connection handshake in frames 1-3 and then in frame 4 we find the sent time object value of 1585599106:

![wireshark](images/wireshark_seed.png)

This was the value used to seed `srand` on line 35 of `main`. For some background, the `rand` function is only a pseudo-random number generator that works in tandem with `srand` ("seed rand") to produce random-like numbers. Users call `srand` first and provide it with an integer value. `srand` then writes to a global variable shared by `rand` and this value will determine what numbers `rand` returns on each call. Given the same seed value to `srand`, `rand` will always return the same sequence of "random" numbers. Providing a time object is a common way to seed `srand` because it is an always changing number that doesn't repeat, so using it as a seed produces good enough randomness for `rand` to be useful. Because we have captured the time object value used to seed `srand`, we can reproduce the string of numbers that were used to XOR the bytes in flag.gif and we can use these to reverse the process and reproduce the flag.  

Firstly, we need to determine how many random numbers we need to produce. We know that the the malware XORs a value against every byte of flag.gif and then stores the result in the output file, and we can see that flag-gif.EnCiPhErEd is 374109 bytes in size:
```
$ ls -l | grep flag-gif.EnCiPhErEd 
-rw-r--r-- 1 tmp tmp  374109 Apr 18 12:21 flag-gif.EnCiPhErEd
```

I wrote a c program to produce 374109 random numbers using our captured time object value:
```c
#include <stdio.h>
#include <stdlib.h>

int main () {
   int i, n;

   // Seed the rand with the value we identifed in the pcap
   srand(1585599106);
  
   // The number of bytes in flag-gif.EnCiPhErEd
   n = 374109;
  
   // Print n number of "random" bytes. Note that we're doing a bitwise AND to only take the least significant byte of the rand() return 
   for( i = 0 ; i < n ; i++ ) {
      printf("%d\n", (rand() & 0xFF));
   }
   
   return(0);
}
```

I compiled it with `gcc get_random_nums.c -o get_random_nums`, then directed the output to keys.txt with `./get_random_nums > keys.txt` We can see with the `head` command that the keys.txt file is filled with lines of bytes. The file is 374109 lines long, with 1 line for every byte of flag-gif.EnCiPhErEd.

```
$ head keys.txt
213
162
91
212
53
154
186
76
249
139
```

I wrote a python script to simultaneously open flag-gif.EnCiPhErEd and keys.txt. It then takes a byte from flag-gif.EnCiPhErEd and a line from keys.txt and XORs them together, reversing the encryption process. It writes the resulting output to a new file, flag.gif.

```python
#! /usr/bin/python3

enc_file = open("flag-gif.EnCiPhErEd", "rb")
key_file = open("keys.txt", "r")
out_file = open("flag.gif", "wb")

while True:
    # read the next byte
    byte = enc_file.read(1)
    
    # if we didn't get a byte, we must be at the end of the file. quit
    if not byte:
        break

    c   = int(byte.hex(),16)
    key = int(key_file.readline().strip())

    # do the xor and then turn the result to a byte for writing
    result = (c ^ key).to_bytes(1,"big")

    # write to our output file
    out_file.write(result)
```

After running the script, we produce the decrypted file and our flag:

![flag](images/flag.gif)

---

### Flag 
```
WPI{It_always_feels_a_little_weird_writing_malware}
```
