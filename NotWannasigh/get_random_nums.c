#include <stdio.h>
#include <stdlib.h>

int main () {
   int i, n;

   // Seed the rand with the value we identifed in the pcap
   srand(1585599106);
  
   // The number of bytes in flag-gif.EnCiPhErEd
   n = 374109;
  
   // Print n number of "random" bytes. Note that we're doing a bitwise AND to only take the least significant byte of the rand() return 
   for( i = 0 ; i < n ; i++ ) {
      printf("%d\n", (rand() & 0xFF));
   }
   
   return(0);
}
