#! /usr/bin/python3

enc_file = open("flag-gif.EnCiPhErEd", "rb")
key_file = open("keys.txt", "r")
out_file = open("flag.gif", "wb")

while True:
    # read the next byte
    byte = enc_file.read(1)
    
    # if we didn't get a byte, we must be at the end of the file. quit
    if not byte:
        break

    c   = int(byte.hex(),16)
    key = int(key_file.readline().strip())

    # do the xor and then turn the result to a byte for writing
    result = (c ^ key).to_bytes(1,"big")

    # write to our output file
    out_file.write(result)
