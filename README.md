## WPICTF 2020

* **CTF Time page:** https://ctftime.org/event/913 
* **Category:** Jeopardy
* **Date:** Fri, 17 April 2020, 21:00 UTC — Sun, 19 April 2020, 21:00 UTC

---

### Solved Challenges
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|dangerous live and malicious code|rev|50|javascript|
|dorsia1|pwn|100|bof|
|NotWannasigh|rev|100|c, srand, rand, xor, static analysis|
|illuminati confirmed|crypto|250|RSA, low exponent value, Chinese Remainder Theorem|

### Unsolved Challenges to Revisit
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|dorsia3|pwn|200|format string attack, return address overwrite|
|luna|crypto|||
